﻿using GameEntities;
using System.Collections.Generic;
using AuxiliaryEntities;

namespace DALInterface
{
    public interface IDALInterface
    {
        void SaveLevel(List<GameObject> map, string levelName);

        bool AddNewScore(string playerName, int playeScore, int levelNum);

        bool SaveGame(List<GameObject> map, string playerName);

        List<GameObject> LoadGame(int saveNum);

        Dictionary<int, List<UserScore>> LoadStatistics();

        List<GameSave> GetSaves();

        Dictionary<int, string> GetLevels();

        List<GameObject> LoadLevel(int levelID);
    }
}
