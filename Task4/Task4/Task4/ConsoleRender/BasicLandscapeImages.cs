﻿namespace Task4.ConsoleRender
{
    public static class BasicLandscapeImages
    {
        public static readonly char[] basicLandscapeImages =
            {(char)39, (char)44, (char)46, (char)58, (char)59, (char)96, (char)247};
    }
}
