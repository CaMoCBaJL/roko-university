﻿using GameEntities;
using AuxiliaryEntities;
using System.Collections.Generic;

namespace CommonInterfaces
{
    public interface IRender
    {
        void ShowUserScores(Dictionary<int, List<UserScore>> scores);

        void ShowMessage(string messageText);

        bool ShowExitDialog();

        bool SaveGameMessage();

        int ChooseLevel(Dictionary<int, string> levels);

        void DrawEnemy(Character enemy);

        void DrawCollectable(Collectable collectable);

        void DrawObstacle(Obstacle obstacle);

        void DrawUser(Point location);

        void GameOver(GameResult result);

        void DrawBasicLandscape(Point location);

        void DrawInterface(GameEntities.ConcreteGameObjectTypes.Player player, int monsterScore, int playerScore);

        void DrawVerticalFieldBorders(Size fieldSize);

        void DrawHorizontalFieldBorders(Size fieldSize);

        ActType ParseUserInput();

        void ClearMap();

        GameSettings WelcomeMenu();

        int ChooseSave(List<GameSave> saves);
    }
}
