﻿using GameEntities;
using GameEntities.ConcreteGameObjectTypes;
using System.Collections.Generic;
using System.Linq;

namespace CommonInterfaces.Interactors
{
    public class EasyGameDifficultyInteractor : IObjectIntercationProceeder
    {
        public List<GameObject> ProceedGroup(List<GameObject> mapCell, out int monsterScore, out int playerScore)
        {
            monsterScore = 0;

            playerScore = 0;

            List<GameObject> objectsToRemove = new List<GameObject>();

            Player player = mapCell.FirstOrDefault(obj => obj is Player) as Player;

            List<Collectable> bonuses = mapCell.FindAll(obj => obj is Collectable).Cast<Collectable>().ToList();

            List<Character> enemies = mapCell.FindAll(obj => obj is Character && obj is not Player).Cast<Character>().ToList();

            Bush bush = mapCell.FirstOrDefault(obj => obj is Bush) as Bush;

            if (player is null)
            {
                if (enemies.Count > 0)
                {
                    bonuses.ForEach(bonus => enemies[0].ApplyBonus(bonus));

                    monsterScore += AddScoresForBonusCollection(enemies[0], bonuses.Count);

                    if (bonuses.Count > 0)
                        objectsToRemove.AddRange(bonuses);
                }
            }
            else
            {
                if (bush is not null)
                    player.TakeDamage(bush.Attack());

                bonuses.ForEach(bonus => player.ApplyBonus(bonus));

                playerScore += AddScoresForBonusCollection(player, bonuses.Count);

                if (bonuses.Count > 0)
                    objectsToRemove.AddRange(bonuses);

                foreach (var enemy in enemies)
                {
                    enemy.TakeDamage(player.Attack());

                    if (enemy.IsAlive)
                        player.TakeDamage(enemy.Attack());
                    else
                    {
                        playerScore += AddScoresForEnemyKill(enemy);

                        objectsToRemove.Add(enemy);
                    }
                }
            }

            return objectsToRemove;
        }

        private int AddScoresForEnemyKill(Character enemy)
        {
            switch (enemy)
            {
                case Wolf:
                    return 3;
                case Fox:
                    return 5;
                case Bear:
                    return 10;
                default:
                    return 1;
            }
        }

        private int AddScoresForBonusCollection(Character character, int bonusesAmount)
        {
            switch (character)
            {
                case Player:
                    return bonusesAmount * 3;
                default:
                    return bonusesAmount * 2;
            }
        }
    }
}
