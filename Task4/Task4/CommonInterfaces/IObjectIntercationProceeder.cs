﻿using AuxiliaryEntities;
using GameEntities;
using System.Collections.Generic;
using System.Linq;

namespace CommonInterfaces
{
    public interface IObjectIntercationProceeder
    {
        List<GameObject> ProceedGroup(List<GameObject> mapCell, out int monsterScore, out int playerScore);
    }
}
