using GameEntities;
using GameEntities.ConcreteGameObjectTypes;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using CommonInterfaces.Interactors;
using GameEntities.Actors;
using AuxiliaryEntities;

namespace UnitTests
{
    public class InitializationTests
    {
        [Test]
        public void WrongPointInitializationTest()
        {
            Assert.Throws(typeof(System.ArgumentException), () => new Point(0, 0));
        }

        [Test]
        public void CorrectPointInitializationTest()
        {
            Assert.DoesNotThrow(() => new Point(10, 10));
        }

        [Test]
        public void WrongSizeInitializationTest()
        {
            Assert.Throws(typeof(System.ArgumentException), () => new Size(0, 0));
        }

        [Test]
        public void CorrectSizeInitializationTest()
        {
            Assert.DoesNotThrow(() => new Size(10, 10));
        }

        [Test]
        public void BonusEffectDurationDecreaseTest()
        {
            Raspberry raspberry = new Raspberry(new Point(1, 1), 1, new CharacterActor(), new Size(20, 20));

            raspberry.Act();

            Assert.That(raspberry.EffectDuration == 0);
        }

        [Test]
        public void CharacterMoveDownTest()
        {
            Bear bear = new Bear(50, 10, 1, new Size(100, 100), new CharacterActor(), new Point(50, 50));

            bear.MoveDown();

            Assert.That(bear.Position.Y == 51);
        }

        [Test]
        public void CharacterMoveUpTest()
        {
            Bear bear = new Bear(50, 10, 1, new Size(100, 100), new CharacterActor(), new Point(50, 50));

            bear.MoveUp();

            Assert.That(bear.Position.Y == 49);
        }

        [Test]
        public void CharacterMoveLeftTest()
        {
            Bear bear = new Bear(50, 10, 1, new Size(100, 100), new CharacterActor(), new Point(50, 50));

            bear.MoveLeft();

            Assert.That(bear.Position.X == 49);
        }

        [Test]
        public void CharacterMoveRightTest()
        {
            Bear bear = new Bear(50, 10, 1, new Size(100, 100), new CharacterActor(), new Point(50, 50));

            bear.MoveRight();

            Assert.That(bear.Position.X == 51);
        }
    }

    public class InteractorTests
    {
        [Test]
        public void TestEnemyAndBonusInteraction()
        {
            Bear bear = new Bear();

            Raspberry raspberry = new Raspberry();

            EasyGameDifficultyInteractor intercator = new EasyGameDifficultyInteractor();

            intercator.ProceedGroup(new List<GameObject>(new GameObject[] { bear, raspberry }), out int monsterScore, out int playerScore);

            Assert.That(monsterScore > 0);
        }

        [Test]
        public void TestPlayerAndBonusInteraction()
        {
            Player player = new Player();

            Raspberry raspberry = new Raspberry();

            EasyGameDifficultyInteractor intercator = new EasyGameDifficultyInteractor();

            intercator.ProceedGroup(new List<GameObject>(new GameObject[] { player, raspberry }), out int monsterScore, out int playerScore);

            Assert.That(playerScore > 0);
        }

        [Test]
        public void EnemyKillTest()
        {
            Player player = new Player(100, 20, 1, new Size(), new CharacterActor(), new Point());

            Bear bear = new Bear(1, 1, 1, new Size(), new CharacterActor(), new Point());

            EasyGameDifficultyInteractor intercator = new EasyGameDifficultyInteractor();

            intercator.ProceedGroup(new List<GameObject>(new GameObject[] { player, bear }), out int monsterScore, out int playerScore);

            Assert.That(playerScore > 0);
        }

        [Test]
        public void PlayerKillTest()
        {
            Player player = new Player(1, 20, 1, new Size(), new CharacterActor(), new Point());

            Bear bear = new Bear(100, 100, 1, new Size(), new CharacterActor(), new Point());

            EasyGameDifficultyInteractor intercator = new EasyGameDifficultyInteractor();

            intercator.ProceedGroup(new List<GameObject>(new GameObject[] { player, bear }), out int monsterScore, out int playerScore);

            Assert.That(!player.IsAlive);
        }

        [Test]
        public void PlayerHitsEnemyTest()
        {
            Player player = new Player(100, 20, 1, new Size(), new CharacterActor(), new Point());

            Bear bear = new Bear(50, 10, 1, new Size(), new CharacterActor(), new Point());

            EasyGameDifficultyInteractor intercator = new EasyGameDifficultyInteractor();

            intercator.ProceedGroup(new List<GameObject>(new GameObject[] { player, bear }), out int monsterScore, out int playerScore);

            Assert.That(bear.Health > 0 && bear.IsAlive);
        }

        [Test]
        public void EnemyHitsPlayerTest()
        {
            Player player = new Player(100, 20, 1, new Size(), new CharacterActor(), new Point());

            Bear bear = new Bear(50, 10, 1, new Size(), new CharacterActor(), new Point());

            EasyGameDifficultyInteractor intercator = new EasyGameDifficultyInteractor();

            intercator.ProceedGroup(new List<GameObject>(new GameObject[] { player, bear }), out int monsterScore, out int playerScore);

            Assert.That(player.IsAlive);
        }
    }

    [TestFixture]
    public class JSONDalTests
    {
        GameEngine.Engine _localEngine;

        [OneTimeSetUp]
        public void CreateEngine()
        {
            _localEngine = new GameEngine.Engine(new Task4.ConsoleRender.Render(System.ConsoleColor.Black), 1, 1, 1, new Size(20, 20), new GameSettings()
            { ConnectionType = GameConnectionType.Local, Difficulty = GameDifficulty.Easy, LevelType = GameLevelType.PreviouslyGenerated });
        }

        [Test]
        public void EngineCreationTest()
        {
            Assert.That(_localEngine is not null);
        }

        [Test]
        public void StaticConstructorTest()
        {
            Assert.That(Directory.Exists(JsonDAL.DAO._localLevelsDir));

            Assert.That(Directory.Exists(JsonDAL.DAO._localSavesDir));
        }

        [Test]
        public void EngineMapCreationTest()
        {
            _localEngine.CreateMap();

            Assert.That(_localEngine.GetType().GetField("_map", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_localEngine) is not null);
        }
    }

    public class SQLDalTests
    {
        GameEngine.Engine _netEngine;

        [OneTimeSetUp]
        public void CreateEngine()
        {
            _netEngine = new GameEngine.Engine(new Task4.ConsoleRender.Render(System.ConsoleColor.Black), 1, 1, 1, new Size(20, 20), new GameSettings()
            { ConnectionType = GameConnectionType.Net, Difficulty = GameDifficulty.Easy, LevelType = GameLevelType.PreviouslyGenerated });
        }

        [Test]
        public void EngineCreationTest()
        {
            Assert.That(_netEngine is not null);
        }

        [Test]
        public void EngineMapCreationTest()
        {
            _netEngine.CreateMap();

            Assert.That(_netEngine.GetType().GetField("_map", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_netEngine) is not null);
        }
    }

}