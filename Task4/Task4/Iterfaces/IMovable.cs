﻿namespace Interfaces
{
    public interface IMovable
    {
        void MoveRight();

        void MoveLeft();

        void MoveUp();

        void MoveDown();
    }
}
