﻿namespace GameEntities
{
    public class GameAction
    {
        public ActType ActType { get; }

        public GameObject GameObject { get; }        


        public GameAction(ActType actType, GameObject gameObject)
        {
            ActType = actType;

            GameObject = gameObject;
        }
    }
}
