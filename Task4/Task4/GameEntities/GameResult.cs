﻿namespace GameEntities
{
    public enum GameResult
    {
        None,
        Win,
        Lose
    }
}
