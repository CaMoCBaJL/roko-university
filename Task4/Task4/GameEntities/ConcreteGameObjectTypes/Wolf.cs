﻿using GameEntities.Actors;
using AuxiliaryEntities;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Wolf : Character
    {
        public Wolf()
        {
            _actor = new CharacterActor();
        }

        public Wolf(CharacterActor actor, Point position, Size fieldSize) : base(25, 1, 15, fieldSize, actor, position)
        {
            _actor = new CharacterActor();
        }

        public Wolf(int health, int baseDamage, int moveStep, Size fieldSize, CharacterActor actor, Point position) : base(health, moveStep, baseDamage, fieldSize, actor, position)
        {
            _actor = new CharacterActor();
        }
    }
}
