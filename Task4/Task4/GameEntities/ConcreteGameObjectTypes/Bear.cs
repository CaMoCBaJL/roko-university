﻿using GameEntities.Actors;
using AuxiliaryEntities;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Bear : Character
    {
        public Bear()
        {
            _actor = new CharacterActor();
        }

        public Bear(CharacterActor actor, Point position, Size fieldSize) : base(50, 3, 25, fieldSize, actor, position)
        {
            _actor = new CharacterActor();
        }

        public Bear(int health, int baseDamage, int moveStep, Size fieldSize, CharacterActor actor, Point position) : base(health, moveStep, baseDamage, fieldSize, actor, position) 
        { 
            _actor = new CharacterActor();
        }
    }
}
