﻿using AuxiliaryEntities;
using GameEntities.Actors;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Player : Character
    {
        [JsonConstructor]
        public Player() { _actor = new CharacterActor(); }

        public Player(CharacterActor actor, Point position, Size fieldSize) : base(100, 1, 5, fieldSize, actor, position) { _actor = new CharacterActor(); }

        public Player(int health, int baseDamage, int moveStep, Size fieldSize, CharacterActor actor, Point position) 
            : base(health, moveStep, baseDamage, fieldSize, actor, position) { _actor = new CharacterActor(); }

        public void Act(ActType actType, List<GameAction> subjectSurroundings)
        {
            if (subjectSurroundings.Exists(obj => obj.ActType == actType))
            {
                if ((_actor as CharacterActor).IsStepAvailable(this, subjectSurroundings.Find(obj => obj.ActType == actType).GameObject))
                    _actor.Act(actType, this);

                return;
            }

            _actor.Act(actType, this);

            CheckBonuses();
        }
    }
}
