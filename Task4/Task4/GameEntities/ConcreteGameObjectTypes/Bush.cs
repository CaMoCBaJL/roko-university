﻿using AuxiliaryEntities;
using GameEntities.Actors;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Bush : Obstacle, Interfaces.IHittable
    {
        public Bush()
        {
            _actor = new ObstacleActor();
        }

        public Bush(ObstacleActor actor, Point position) : base(actor, position)
        {
            _actor = new ObstacleActor();
        }

        public Bush(ObstacleActor actor, Point position, Size size) : base(position, size, actor)
        {
            _actor = new ObstacleActor();
        }

        public int Attack()
        => 5;
    }
}
