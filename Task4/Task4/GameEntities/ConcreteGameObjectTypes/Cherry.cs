﻿using GameEntities.Actors;
using System;
using AuxiliaryEntities;
using Newtonsoft.Json;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Cherry : Collectable
    {
        [JsonConstructor]
        public Cherry()
        {
            _actor = new CollectableActor();

            _effectOnUser += (player) => player.BaseDamage = player.Health + 5;

            _effectOnEnemy += (enemy) => enemy.Health = enemy.BaseDamage + 4;
        }

        public Cherry(CollectableActor actor, Point position, Size size) 
            : base(position, (player) => player.BaseDamage = player.Health + 5, (enemy) => enemy.Health = enemy.BaseDamage + 4, int.MaxValue, actor, size)
        {
            _actor = new CollectableActor();

            Name = "Cherry";
        }

        public Cherry(Action<Character> userEffect, Action<Character> enemyEffect, int effectDuration, CollectableActor actor, Point position, Size size)
            : base(position, userEffect, enemyEffect, effectDuration, actor, size)
        {
            _actor = new CollectableActor();

            Name = "Cherry";
        }

        public Cherry(Point position, int effectDuration, CharacterActor actor, Size fieldSize) : base(position, effectDuration, actor, fieldSize)
        {
            _effectOnUser += (player) => player.BaseDamage = player.Health + 5;

            _effectOnEnemy += (enemy) => enemy.Health = enemy.BaseDamage + 4;

            Name = "Cherry";

            _actor = new CollectableActor();
        }
    }
}

