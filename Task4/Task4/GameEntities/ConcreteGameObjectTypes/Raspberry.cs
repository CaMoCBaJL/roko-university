﻿using AuxiliaryEntities;
using GameEntities.Actors;
using Newtonsoft.Json;
using System;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Raspberry : Collectable
    {
        [JsonConstructor]
        public Raspberry()
        {
            Name = "Raspberry";

            _actor = new CollectableActor();

            _effectOnUser += (player) => player.BaseDamage = player.BaseDamage + 20;

            _effectOnEnemy += (enemy) => enemy.BaseDamage = enemy.BaseDamage + 10;
        }

        public Raspberry(CollectableActor actor, Point position, Size size) 
            : base(position, (player) => player.Health = player.BaseDamage * 2, (enemy) => enemy.BaseDamage = enemy.BaseDamage * 2, 7, actor, size) 
        { 
            Name = "Raspberry";

            _actor = new CollectableActor();
        }

        public Raspberry(Action<Character> userEffect, Action<Character> enemyEffect, int effectDuration, CollectableActor actor, Point position, Size size)
            : base(position, userEffect, enemyEffect, effectDuration, actor, size) 
        {
            Name = "Raspberry";

            _actor = new CollectableActor();
        }

        public Raspberry(Point position, int effectDuration, CharacterActor actor, Size fieldSize) : base(position, effectDuration, actor, fieldSize)
        {
            _effectOnUser += (player) => player.BaseDamage = player.Health + 20;

            _effectOnEnemy += (enemy) => enemy.Health = enemy.BaseDamage + 10;

            Name = "Pineapple";

            _actor = new CollectableActor();
        }

        public override void RemoveBonusEffect(Character subject)
        {
            if (subject is Bear)
                subject.BaseDamage /= 2;

            base.RemoveBonusEffect(subject);
        }
    }
}
