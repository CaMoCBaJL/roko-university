﻿using AuxiliaryEntities;
using GameEntities.Actors;
using Newtonsoft.Json;
using System;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class BlueBerry : Collectable
    {
        [JsonConstructor]
        public BlueBerry()
        {
            _actor = new CollectableActor();

            Name = "BlueBerry";

            _effectOnUser += (player) => player.Health = player.BaseDamage * 2;

            _effectOnEnemy += (enemy) => enemy.BaseDamage = enemy.BaseDamage * 2;
        }

        public BlueBerry(CollectableActor actor, Point position, Size size)
            : base(position, (player) => player.Health = player.BaseDamage * 2, (enemy) => enemy.BaseDamage = enemy.BaseDamage * 2, 13, actor, size)
        {
            _actor = new CollectableActor();

            Name = "Blueberry";
        }

        public BlueBerry(Action<Character> userEffect, Action<Character> enemyEffect, int effectDuration, CollectableActor actor, Point position, Size size)
            : base(position, userEffect, enemyEffect, effectDuration, actor, size)
        {
            _actor = new CollectableActor();

            Name = "Blueberry";
        }

        public BlueBerry(Point position, int effectDuration, CharacterActor actor, Size fieldSize) : base(position, effectDuration, actor, fieldSize)
        {
            _effectOnUser += (player) => player.Health = player.BaseDamage * 2;

            _effectOnEnemy += (enemy) => enemy.BaseDamage = enemy.BaseDamage * 2;

            Name = "BlueBerry";

            _actor = new CollectableActor();
        }
    }
}
