﻿using AuxiliaryEntities;
using GameEntities.Actors;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Wall : Obstacle
    {
        public Wall()
        {
            _actor = new ObstacleActor();
        }

        public Wall(ObstacleActor actor, Point position) : base(actor, position)
        {
            _actor = new ObstacleActor();
        }

        public Wall(ObstacleActor actor, Point position, Size size) : base(position, size, actor)
        {
            _actor = new ObstacleActor();
        }
    }
}
