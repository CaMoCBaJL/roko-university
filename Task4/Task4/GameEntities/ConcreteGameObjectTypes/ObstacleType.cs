﻿namespace GameEntities.ConcreteGameObjectTypes
{
    public enum ObstacleType
    {
        Bush = 1,
        Rock = 2,
        Wall = 3
    }
}
