﻿namespace GameEntities
{
    public enum CharacterType
    {
        Bear = 1,
        Fox = 2,
        Wolf = 3,
        Player = 4
    }
}
