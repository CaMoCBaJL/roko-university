﻿using AuxiliaryEntities;
using GameEntities.Actors;

namespace GameEntities.ConcreteGameObjectTypes
{
    public class Rock : Obstacle
    {
        public Rock()
        {
            _actor = new ObstacleActor();
        }

        public Rock(ObstacleActor actor, Point position) : base(actor, position)
        {
            _actor = new ObstacleActor();
        }

        public Rock (ObstacleActor actor, Point location, Size size) : base(location, size, actor)
        {
            _actor = new ObstacleActor();
        }
    }
}
