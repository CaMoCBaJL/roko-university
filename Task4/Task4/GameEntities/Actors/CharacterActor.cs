﻿using System.Collections.Generic;
using RandomGenerator;

namespace GameEntities.Actors
{
    public class CharacterActor : BaseActor
    {
        public override void ChooseAction(List<GameAction> objectSurroundings, GameObject subject)
        {
            var character = subject as Character;

            character.CheckBonuses();

            foreach (var item in objectSurroundings)
            {
                if (IsStepAvailable(character, item.GameObject))
                {
                    for (int i = 0; i < character.MoveStep; i++)
                        Act(item.ActType, character);

                    return;
                }
            }

            Act((ActType)RndGen.rndGenerator.Next(1, 8), subject);
        }

        public override void Act(ActType direction, GameObject subject)
        {
            Character character = subject as Character;

            switch (direction)
            {
                case ActType.MoveUp:
                    character.MoveUp();
                    break;
                case ActType.MoveDown:
                    character.MoveDown();
                    break;
                case ActType.MoveRight:
                    character.MoveRight();
                    break;
                case ActType.MoveLeft:
                    character.MoveLeft();
                    break;
                case ActType.MoveLeftDown:
                    character.MoveLeft();
                    character.MoveDown();
                    break;
                case ActType.MoveLeftUp:
                    character.MoveLeft();
                    character.MoveUp();
                    break;
                case ActType.MoveRightDown:
                    character.MoveRight();
                    character.MoveDown();
                    break;
                case ActType.MoveRightUp:
                    character.MoveRight();
                    character.MoveUp();
                    break;
                default:
                    break;
            }
        }

        public bool IsStepAvailable(Character character, GameObject nextObejct)
        {
            if (UnderBlueBerryEffect(character))
                return true;
            else
            {
                if (nextObejct is Obstacle)
                {
                    if (nextObejct is ConcreteGameObjectTypes.Bush && character is ConcreteGameObjectTypes.Player)
                        return true;
                    else
                        return false;
                }

                return true;
            }
        }

        bool UnderBlueBerryEffect(Character character)
        {
            foreach (var item in character.GetActiveBonuses())
            {
                switch (item)
                {
                    case ConcreteGameObjectTypes.BlueBerry:
                        return true;
                }
            }

            return false;
        }
    }
}
