﻿using System.Collections.Generic;

namespace GameEntities.Actors
{
    public abstract class BaseActor
    {
        public abstract void ChooseAction(List<GameAction> objectSurroundings, GameObject subject);

        public abstract void Act(ActType actionType, GameObject subject);
    }
}
