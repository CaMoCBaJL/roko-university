﻿using AuxiliaryEntities;
using GameEntities.Actors;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GameEntities
{
    public abstract class GameObject
    {
        protected BaseActor _actor;

        [JsonProperty]
        public Point Position { get; private set; }

        [JsonProperty]
        public Size Size { get; private set; }

        [JsonConstructor]
        public GameObject() { }

        public GameObject(Point position, BaseActor actor, Size fieldSize)
        {
            Position = position;

            Size = fieldSize;

            _actor = actor;
        }

        public GameObject(BaseActor actor, Point location)
        {
            _actor = actor;

            Position = location;

            Size = new Size();
        }

        protected void SetSize(Size newSize) => Size = newSize;

        protected void SetPosition(Point newPosition) => Position = newPosition;

        public abstract void Act(List<GameAction> surroundings);

        public void ChangeFieldSize(Size correctSize) => Size = correctSize;

        public Size GetFiledSize() => new Size(Size.Height, Size.Width);

        public abstract void AddActor();
    }
}
