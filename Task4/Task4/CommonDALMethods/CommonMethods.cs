﻿using System;

namespace CommonDALMethods
{
    public static class CommonMethods
    {
        public static List<GameObject> LoadObjectsFromFile(int fileIndx, string pathToDirectory)
        {
            List<GameObject> result = new List<GameObject>();

            var saveData = File.ReadAllLines(Directory.GetFiles(pathToDirectory)[fileIndx]);

            for (int i = 0; i < saveData.Length; i += 2)
            {
                LoadGameObject(result, saveData[i], saveData[i + 1]);
            }

            return result;
        }

    }
}
