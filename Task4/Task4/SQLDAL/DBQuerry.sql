﻿USE master
GO
CREATE DATABASE [GameObjectsStorage]
GO

USE GameObjectsStorage
GO

CREATE TABLE [dbo].[UserScore]
(
	[ScoreID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[PlayerName] NVARCHAR(255),
	[PlayerScore] INT,
	[LevelNum] TINYINT
)
GO

CREATE TABLE [dbo].[GameSave]
(
	[ID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[SaveDate] DATETIME NOT NULL,
	[PlayerName] NVARCHAR(255) NOT NULL
)
GO

CREATE TABLE [dbo].[Object]
(
	[ID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[SaveID] INT FOREIGN KEY REFERENCES [dbo].[GameSave](ID) ON DELETE CASCADE ON UPDATE CASCADE,
	[GameFieldHeight] INT NOT NULL,
	[GameFieldWidth] INT NOT NULL,
	[X] INT NOT NULL,
	[Y] INT NOT NULL
)
GO

CREATE TABLE [dbo].[Collectables]
(
	[ID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Type] TINYINT NOT NULL,
	[EffectDuration] INT NOT NULL,
	[PositionID] INT FOREIGN KEY REFERENCES [dbo].[Object](ID) ON DELETE CASCADE ON UPDATE CASCADE,
)
GO

CREATE TABLE [dbo].[Characters]
(
	[ID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Type] TINYINT NOT NULL,
	[PositionID] INT FOREIGN KEY REFERENCES [dbo].[Object](ID) ON DELETE CASCADE ON UPDATE CASCADE,
	[MoveStep] INT NOT NULL,
	[Damage] INT NOT NULL,
	[Health] INT NOT NULL
)
GO

CREATE TABLE [dbo].[ActiveBonuses]
(
	[CharacterID] INT FOREIGN KEY REFERENCES Characters(ID),
	[BonusID] INT FOREIGN KEY REFERENCES Collectables(ID),
	CONSTRAINT ActiveBonusesAreUnique UNIQUE (CharacterID, BonusID)
)
GO

CREATE TABLE [dbo].[Obstacle]
(
	[ID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[Type] TINYINT NOT NULL,
	[PositionID] INT FOREIGN KEY REFERENCES [dbo].[Object](ID) ON DELETE CASCADE ON UPDATE CASCADE,
)
GO

CREATE TABLE [dbo].[GameLevel]
(
	[ID] INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[LevelData] NVARCHAR(MAX),
	[LevelName] NVARCHAR(255) UNIQUE
)
GO

CREATE PROCEDURE GetAllObstacles
AS
BEGIN
	SELECT [dbo].[Obstacle].[ID], [dbo].[Obstacle].[Type], [dbo].[Object].[SaveID], [dbo].[Object].[X], [dbo].[Object].[Y], 
	[dbo].[Object].[GameFieldHeight], [dbo].[Object].[GameFieldWidth] 
	FROM [dbo].[Obstacle] JOIN [dbo].[Object] 
	ON [dbo].[Obstacle].[PositionID] = [dbo].[Object].[ID]
END
GO

CREATE PROCEDURE GetAllCharacters
AS
BEGIN
	SELECT [dbo].[Characters].[ID], [dbo].[Characters].[Type], [dbo].[Object].[SaveID], [dbo].[Characters].[Damage],
	[dbo].[Characters].[Health], [dbo].[Characters].[MoveStep], [dbo].[Object].[X], [dbo].[Object].[Y], 
	[dbo].[Object].[GameFieldHeight], [dbo].[Object].[GameFieldWidth] 
	FROM [dbo].[Characters] JOIN [dbo].[Object] 
	ON [dbo].[Characters].[PositionID] = [dbo].[Object].[ID];
END 
GO

CREATE PROCEDURE GetStatistics
@LevelNum TINYINT
AS
BEGIN
	SELECT [dbo].[UserScore].[PlayerName], [dbo].[UserScore].[PlayerScore] FROM [dbo].[UserScore] WHERE [dbo].[UserScore].[LevelNum] = @LevelNum
END
GO

CREATE PROCEDURE AddUserScore
@PlayerName NVARCHAR(255),
@PlayerScore INT,
@LevelNum TINYINT
AS
BEGIN
	BEGIN TRANSACTION
		IF ((select MIN([dbo].[UserScore].[PlayerScore]) from [dbo].[UserScore]) < @PlayerScore OR (select MIN([dbo].[UserScore].[PlayerScore]) from [dbo].[UserScore]) is null)
			BEGIN
				IF ((SELECT COUNT([dbo].[UserScore].[PlayerScore]) FROM [dbo].[UserScore] WHERE [dbo].[UserScore].[LevelNum] = @LevelNum) = 10)
					BEGIN
						delete from [dbo].[UserScore] where [dbo].[UserScore].[ScoreID] = ANY(select top(1) [dbo].[UserScore].[ScoreID] FROM [dbo].[UserScore]
						WHERE [dbo].[UserScore].[PlayerScore] = (select MIN([dbo].[UserScore].[PlayerScore]) from [dbo].[UserScore]))
						AND [dbo].[UserScore].[LevelNum] = @LevelNum
				
						IF (@@ERROR <> 0)
							ROLLBACK
					END
				
				INSERT INTO [dbo].[UserScore]([dbo].[UserScore].[LevelNum], [dbo].[UserScore].[PlayerScore], [dbo].[PlayerName]) VALUES (@LevelNum, @PlayerScore, @PlayerName)
				
				IF (@@ERROR <> 0)
					ROLLBACK
			END
	
	COMMIT
END
GO

CREATE PROCEDURE GetActiveBonuses
AS 
BEGIN
	SELECT [dbo].[ActiveBonuses].[CharacterID], [dbo].[Collectables].[ID], [dbo].[Collectables].[Type], 
    [dbo].[Object].[SaveID], [dbo].[Collectables].[EffectDuration], [dbo].[Object].[GameFieldHeight], [dbo].[Object].[GameFieldWidth] 
	FROM [dbo].[Characters] JOIN [dbo].[ActiveBonuses]
	ON  [dbo].[ActiveBonuses].[CharacterID] = [dbo].[Characters].[ID]
	JOIN [dbo].[Collectables]
	ON [dbo].[Collectables].[ID] = [dbo].[ActiveBonuses].[BonusID]
	JOIN [dbo].[Object]
	ON [dbo].[Object].[ID] = [dbo].[Collectables].[PositionID]
END
GO

CREATE PROCEDURE GetStaticBonuses
AS
BEGIN
	SELECT [dbo].[Collectables].[ID], [dbo].[Collectables].[Type], [dbo].[Object].[SaveID], [dbo].[Collectables].[EffectDuration], 
	[dbo].[Object].[X], [dbo].[Object].[Y], [dbo].[Object].[GameFieldHeight], [dbo].[Object].[GameFieldWidth] 
	FROM [dbo].[Collectables] JOIN [dbo].[Object]
	ON [dbo].[Collectables].[PositionID] = [dbo].[Object].[ID]
	WHERE [dbo].[Collectables].[ID] < ALL(SELECT [dbo].[ActiveBonuses].[BonusID] FROM [dbo].[ActiveBonuses])
END
GO

CREATE PROCEDURE GetSaves
AS
BEGIN
	SELECT * FROM [dbo].[GameSave]
END
GO

CREATE PROCEDURE LoadSave
@SaveId INT
AS
BEGIN
	CREATE TABLE #SaveCharacters
	(
		[ID] INT,
		[Type] INT,
		[SaveId] INT,
		[Damage] INT,
		[Health] INT,
		[MoveStep] INT,
		[X] INT,
		[Y] INT,
		[GameFieldWidth] INT,
		[GameFieldHeight] INT
	)

	CREATE TABLE #ActiveBonuses
	(
		[CharacterID] INT,
		[ID] INT,
		[Type] INT,
		[SaveId] INT,
		[EffectDuration] INT,
		[GameFieldWidth] INT,
		[GameFieldHeight] INT
	)

	CREATE TABLE #Obstacles
	(
		[ID] INT,
		[Type] INT,
		[SaveId] INT,
		[X] INT,
		[Y] INT,
		[GameFieldWidth] INT,
		[GameFieldHeight] INT
	)

	CREATE TABLE #MapBonuses
	(
		[ID] INT,
		[Type] INT,
		[SaveId] INT,
		[EffectDuration] INT,
		[X] INT,
		[Y] INT,
		[GameFieldWidth] INT,
		[GameFieldHeight] INT
	)

	INSERT INTO #SaveCharacters EXEC GetAllCharacters

	INSERT INTO #ActiveBonuses EXEC GetActiveBonuses

	INSERT INTO #Obstacles EXEC GetAllObstacles

	INSERT INTO #MapBonuses EXEC GetStaticBonuses

	SELECT * FROM #SaveCharacters WHERE SaveId = @SaveId

	SELECT * FROM #ActiveBonuses WHERE SaveId = @SaveId

	SELECT * FROM #Obstacles WHERE SaveId = @SaveId

	SELECT * FROM #MapBonuses WHERE SaveId = @SaveId

	DROP TABLE #SaveCharacters

	DROP TABLE #ActiveBonuses

	DROP TABLE #MapBonuses

	DROP TABLE #Obstacles
END
GO

CREATE PROCEDURE SavePosition
@X INT,
@Y INT,
@SaveID INT,
@FieldWidth INT,
@FieldHeight INT
AS 
BEGIN
	IF (
	(SELECT [dbo].[Object].[ID] FROM [dbo].[Object] WHERE [dbo].[Object].[X] = @X AND [dbo].[Object].[Y] = @Y AND [dbo].[Object].[SaveID] = @SaveID)
	is null)
		BEGIN
			INSERT INTO [dbo].[Object]([dbo].[Object].[X], [dbo].[Object].[Y], [dbo].[Object].[SaveID], [dbo].[Object].[GameFieldWidth], [dbo].[Object].[GameFieldHeight])
			VALUES (@X, @Y, @SaveID, @FieldWidth, @FieldHeight)
		END
	
	RETURN (SELECT [dbo].[Object].[ID] FROM [dbo].[Object] WHERE [dbo].[Object].[X] = @X AND [dbo].[Object].[Y] = @Y AND [dbo].[Object].[SaveID] = @SaveID)
END
GO

CREATE PROCEDURE SaveCollectable
@Type TINYINT,
@SaveID INT,
@EffectDuration INT,
@X INT,
@Y INT,
@FieldWidth INT,
@FieldHeight INT
AS
BEGIN
	BEGIN TRANSACTION
		DECLARE @PositionID INT

		EXEC @PositionID = SavePosition @X, @Y, @SaveID, @FieldWidth, @FieldHeight

		IF (@@ERROR <> 0)
			ROLLBACK

		INSERT INTO [dbo].[Collectables] VALUES (@Type, @EffectDuration, @PositionID)

		IF (@@ERROR <> 0)
			ROLLBACK
	COMMIT
END
GO

CREATE PROCEDURE SaveObstacle
@Type TINYINT,
@SaveID INT,
@X INT,
@Y INT,
@FieldWidth INT,
@FieldHeight INT
AS 
BEGIN
	BEGIN TRANSACTION
		DECLARE @PositionID INT

		EXEC @PositionID = SavePosition @X, @Y, @SaveID, @FieldWidth, @FieldHeight

		IF (@@ERROR <> 0)
			ROLLBACK
		
		INSERT INTO [dbo].[Obstacle] VALUES(@Type, @PositionID)
		
		IF (@@ERROR <> 0)
			ROLLBACK

	COMMIT
END
GO

CREATE PROCEDURE SaveCharacterBonus
@CharacterID INT,
@Type TINYINT,
@SaveID INT,
@EffectDuration INT,
@FieldWidth INT,
@FieldHeight INT
AS
BEGIN
	BEGIN TRANSACTION
		DECLARE @PositionID INT

				EXEC @PositionID = SavePosition -1, -1, @SaveID, @FieldWidth, @FieldHeight



		INSERT INTO [dbo].[Collectables] VALUES(@Type, @EffectDuration, @PositionID)

		IF (@@ERROR <> 0)
			ROLLBACK

		INSERT INTO [dbo].[ActiveBonuses] VALUES(@CharacterID, @@IDENTITY)

		IF (@@ERROR <> 0)
			ROLLBACK

	COMMIT
END
GO

CREATE PROCEDURE SaveCharacter
@Type INT,
@SaveId INT,
@Damage INT,
@Health INT,
@MoveStep INT,
@X INT,
@Y INT,
@FieldWidth INT,
@FieldHeight INT
AS 
BEGIN
	BEGIN TRANSACTION
		DECLARE @PositionID INT

		EXEC @PositionID = SavePosition @X, @Y, @SaveId, @FieldWidth, @FieldHeight

		IF (@@ERROR <> 0)
		ROLLBACK

		INSERT INTO [dbo].[Characters] VALUES(@Type, @PositionID, @MoveStep, @Damage, @Health) 

		IF (@@ERROR <> 0)
		ROLLBACK

	COMMIT
END
GO

CREATE PROCEDURE RemoveActiveBonuses
@SaveID INT
AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM [dbo].[ActiveBonuses]
	WHERE [dbo].[ActiveBonuses].[BonusID] IN 
	(SELECT [dbo].[Collectables].[ID] 
	FROM [dbo].[Collectables] JOIN [dbo].[Object] ON [dbo].[Collectables].[PositionID] = [dbo].[Object].[ID]
	WHERE [dbo].[Object].[SaveID] = @SaveID)
	
	DELETE FROM [dbo].[Collectables] 
	WHERE [dbo].[Collectables].[ID] IN (SELECT [dbo].[Collectables].[ID] 
	FROM [dbo].[Collectables] JOIN [dbo].[Object] ON [dbo].[Collectables].[PositionID] = [dbo].[Object].[ID]
	WHERE [dbo].[Object].[SaveID] = @SaveID) 
	
	IF (@@ERROR <> 0)
	ROLLBACK

	COMMIT
END
GO

CREATE PROCEDURE RemoveSave
@SaveID INT
AS 
BEGIN
	BEGIN TRANSACTION
		EXEC RemoveActiveBonuses @SaveID

		IF (@@ERROR <> 0)
			ROLLBACK

		DELETE FROM [dbo].[GameSave] WHERE [dbo].[GameSave].[ID] = @SaveID

		IF (@@ERROR <> 0)
			ROLLBACK
	COMMIT
END
GO

CREATE PROCEDURE SaveGame
@PlayerName NVARCHAR(255),
@SaveDateTime DATETIME
AS
BEGIN
	BEGIN TRANSACTION

		DECLARE @SaveID INT
		SELECT @SaveID = [dbo].[GameSave].[ID] FROM [dbo].[GameSave] WHERE [dbo].[GameSave].[PlayerName] = @PlayerName

		IF(@SaveID IS NOT NULL)
			BEGIN
				EXEC RemoveSave @SaveID
			END

		INSERT INTO [dbo].[GameSave]([dbo].[GameSave].[SaveDate], [dbo].[GameSave].[PlayerName])  VALUES(@SaveDateTime, @PlayerName)

		IF (@@ERROR <> 0)
			ROLLBACK

	COMMIT

	RETURN @@IDENTITY
END
GO

CREATE PROCEDURE GetLevels
AS
BEGIN
	SELECT [dbo].[GameLevel].[ID], [dbo].[GameLevel].[LevelName] FROM [dbo].[GameLevel]
END
GO

CREATE PROCEDURE SaveLevel
@LevelName NVARCHAR(255),
@JSONLevelData NVARCHAR(MAX)
AS
BEGIN
	INSERT INTO [dbo].[GameLevel]([dbo].[GameLevel].[LevelName], [dbo].[GameLevel].[LevelData]) VALUES (@LevelName, @JSONLevelData)
END
GO


CREATE PROCEDURE LoadLevel
@LevelID INT
AS
BEGIN
	SELECT [dbo].[GameLevel].[LevelData] FROM [dbo].[GameLevel] WHERE [dbo].[GameLevel].[ID] = @LevelID
END
GO