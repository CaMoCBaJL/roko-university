﻿using System;
using System.IO;
using System.Linq;
using AuxiliaryEntities;

namespace SQLDAL
{
    static class Common
    {
        private static string _connectionString;

        internal static string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                    _connectionString = GetConnectionString();

                return _connectionString;
            }
        }


        static string GetConnectionString()
        {
            var config = AppDomain.CurrentDomain.BaseDirectory.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            for (int i = 0; i < 3; i++)
                config.Remove(config.Last());

            var neededDir = new DirectoryInfo(Path.Combine(config.ToArray())).GetFiles();

            foreach (var item in neededDir)
            {
                if (item.Name == "appsettings.json")
                {
                    var data = File.ReadAllText(item.FullName).Split(new char[] { '{', '}', ':' }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < data.Length; i++)
                    {
                        if (data[i].Contains("default"))
                            return data[i + 1].Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("\"", string.Empty);
                    }
                }
            }

            throw new FormatException("Application settings have wrong format! No connection string found!");
        }

        public static readonly Point activeBonusLocation = new Point(1, 1);
    }
}
