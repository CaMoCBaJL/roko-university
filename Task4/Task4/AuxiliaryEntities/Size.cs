﻿namespace AuxiliaryEntities
{
    public class Size
    {
        int height;

        int width;


        public int Height { get => height;
            set
            {
                if (value > 0)
                    height = value;
                else
                    throw new System.ArgumentException("Height can't be below zero!");
            }
        }

        public int Width
        {
            get => width;
            set
            {
                if (value > 0)
                    width = value;
                else
                    throw new System.ArgumentException("Height can't be below zero!");
            }
        }

        
        public Size(){ }

        public Size(int height, int width)
        {
            Height = height;

            Width = width;
        }

        public override bool Equals(object obj)
        {
            Size newSize = obj as Size;

            return Height == newSize.height && Width == newSize.width;
        }

        public override int GetHashCode()
        {
            return height.GetHashCode() ^ width.GetHashCode();
        }
    }
}
