﻿namespace AuxiliaryEntities
{
    public class GameSettings
    {
        public GameDifficulty Difficulty { get; set; }

        public GameLevelType LevelType { get; set; }

        public GameConnectionType ConnectionType { get; set; }
    }
}
