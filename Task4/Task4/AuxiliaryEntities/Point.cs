﻿namespace AuxiliaryEntities
{
    public class Point
    {
        int x;
        int y;


        public int X
        {
            get => x;
            set
            {
                if (value > 0)
                    x = value;
                else
                    throw new System.ArgumentException("X coordinate can't be below zero!");
            }
        }

        public int Y
        {
            get => y;
            set
            {
                if (value > 0)
                    y = value;
                else
                    throw new System.ArgumentException("Y coordinate can't be below zero!");
            }
        }


        public Point() { }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            Point pointToCompare = obj as Point;

            return X == pointToCompare.X && Y == pointToCompare.Y;
        }

        public override int GetHashCode()
        => X.GetHashCode() ^ Y.GetHashCode();

        public static bool operator ==(Point p1, Point p2)
        => p1.Equals(p2);

        public static bool operator !=(Point p1, Point p2)
        => p1.Equals(p2);
    }
}
