﻿namespace AuxiliaryEntities
{
    public class GameSave
    {
        public int SaveID { get; set; }

        public string SaveDate { get; set; }

        public string PlayerName { get; set; }


        public override string ToString()
        => $"SaveId: {SaveID} Player name: {PlayerName} Save date: {SaveDate}";
        
    }
}
