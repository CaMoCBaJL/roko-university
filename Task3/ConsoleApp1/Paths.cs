﻿using System;
using System.IO;

namespace ConsoleApp1
{
    class Paths
    {
        public static readonly string wordsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "words.txt");

        public static readonly string symbolsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "symbols.txt");

        public static readonly string wordsFile1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "words1.txt");

        public static readonly string symbolsFile1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "symbols1.txt");
    }
}
