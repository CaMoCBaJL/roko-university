﻿using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class LogicController
    {
        //optimal algorythm ~ O(n)
        public static void ExecuteOptimalAlgorithm(string dirPath, string fileExtention)
        {
            Dictionary<char, int> symbols = new Dictionary<char, int>();

            Dictionary<string, int> words = new Dictionary<string, int>();

            string word = string.Empty;

            foreach (var file in new DirectoryInfo(dirPath).GetFiles(fileExtention, SearchOption.AllDirectories))
            {
                string sourceText = GetText(file);
                for (int i = 0; i < sourceText.Length; i++)
                {
                    char currentSymbol = char.ToLower(sourceText[i]);

                    if (symbols.ContainsKey(currentSymbol))
                        symbols[currentSymbol]++;
                    else
                        symbols.Add(currentSymbol, 1);

                    if (char.IsPunctuation(sourceText[i]) || char.IsWhiteSpace(sourceText[i]) || char.IsPunctuation(sourceText[i]))
                    {
                        string currentWord = word.ToLower();

                        if (words.ContainsKey(currentWord))
                            words[currentWord]++;
                        else
                            words.Add(currentWord, 1);

                        word = string.Empty;
                    }
                    else
                        word = word + sourceText[i];
                }
            }

            symbols.Remove('\r');

            symbols.Remove('\n');

            LogResult(words, symbols, Paths.symbolsFile1, Paths.wordsFile1);
        }

        private static string GetText(FileInfo file)
        => File.ReadAllText(file.FullName);
       

        // not optimal alg ~ O(n^2)
        public static void ExecuteNotOptimalAlgorithm(string dirPath, string fileExtention)
        {
            Dictionary<string, int> words = new Dictionary<string, int>();

            Dictionary<char, int> symbols = new Dictionary<char, int>();

            foreach (var file in new DirectoryInfo(dirPath).GetFiles(fileExtention, SearchOption.AllDirectories))
            {
                //O(3n)
                string sourceText = File.ReadAllText(file.FullName).ToLower().Replace("\r", " ").Replace("\n", " ");

                List<string> fileWords = Regex.Replace(sourceText, @"[^\w\d\s]", "").Split(new char[] { ' ' },
                    System.StringSplitOptions.RemoveEmptyEntries).ToList();

                //O(n)
                fileWords.ForEach((word) =>
                {
                    if (words.ContainsKey(word))
                        words[word]++;
                    else
                        words.Add(word, 1);
                });
                //O(n)
                sourceText.ToCharArray().ToList().ForEach((symbol) =>
                {
                    if (symbols.ContainsKey(symbol))
                        symbols[symbol]++;
                    else
                        symbols.Add(symbol, 1);
                });
            }

            LogResult(words, symbols, Paths.symbolsFile, Paths.wordsFile);
        }

        private static void LogResult(Dictionary<string, int> words, Dictionary<char, int> symbols,
            string symbolsLogFileName, string wordsLogFileName)
        {
            using (StreamWriter wordWriter = new StreamWriter(wordsLogFileName))
                foreach (var item in words)
                {
                    wordWriter.WriteLine($"{item.Key} - {item.Value}");
                }

            using (StreamWriter symbolWriter = new StreamWriter(symbolsLogFileName))
                foreach (var item in symbols)
                {
                    symbolWriter.WriteLine($"{item.Key} - {item.Value}");
                }
        }
    }
}
