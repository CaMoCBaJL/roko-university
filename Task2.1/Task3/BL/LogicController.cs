﻿using Entities;
using System.Collections.Generic;
using DAL;
using FileTypeDefiner;
using System;
using System.Linq;


namespace BL
{
    public class LogicController
    {
        static readonly char[] stringSeparators = { '\r', '\n' };

        public IEnumerable<UnitOfChange> SearchForDifference(string pathToDir1, string pathToDir2)
        {
            List<UnitOfChange> differences = new List<UnitOfChange>();

            List<FileEntity> firstDirContent = DAO.GetDirectoryContent(pathToDir1);

            List<FileEntity> secondDirContent = DAO.GetDirectoryContent(pathToDir2);

            new Determinant().DefineFileTypes(firstDirContent, secondDirContent);

            FindUniqueFiles(firstDirContent, secondDirContent, differences);

            foreach (var difference in FindFilesDifferences(FileType.Binary, DifferenceType.BinaryFileDifference,
                firstDirContent, secondDirContent, BinaryComparer))
                differences.Add(difference);

            foreach (var difference in FindFilesDifferences(FileType.Text, DifferenceType.TextFileContentDifference,
                firstDirContent, secondDirContent, TextComparer))
                differences.Add(difference);

            return differences;
        }

        IEnumerable<UnitOfChange> FindFilesDifferences<T>(FileType fileType, DifferenceType differenceType,
            List<FileEntity> firstDirContent, List<FileEntity> secondDirContent, Func<FileEntity, FileEntity, IEnumerable<T>> comparer)
            where T: FileChangeEntity
        {
            List<UnitOfChange> differences = new List<UnitOfChange>();

            foreach (var item in firstDirContent.FindAll((file) => file.Type == fileType))
            {
                var changeData = comparer(item, secondDirContent.Find((file) => file.Equals(item)));

                if (changeData != null)
                    differences.Add(new UnitOfChange() { ChangeType = differenceType, Changes = changeData });
            }

            return differences;
        }

        IEnumerable<FileChangeEntity> BinaryComparer(FileEntity file1, FileEntity file2)
        {
            if (DAO.GetFileLastModifyTime(file1.FileName) > DAO.GetFileLastModifyTime(file2.FileName))
                return new[] { new FileChangeEntity() { SecondFileName = file2.FileName, FirstFileName = file1.FileName} };
            else
                return new[] { new FileChangeEntity() { SecondFileName = file1.FileName, FirstFileName = file2.FileName} };
        }

        IEnumerable<TextFileChange> TextComparer(FileEntity file1, FileEntity file2)
        {
            List<TextFileChange> result = new List<TextFileChange>();

            var firstFileData = file1.Content.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            var secondFileData = file2.Content.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            var intersectionData = secondFileData.Intersect(firstFileData);

            //removed data
            foreach (var item in firstFileData)
            {
                if (!intersectionData.Contains(item))
                    result.Add(new TextFileChange() {FirstFileName = file1.FileName, SecondFileName = file2.FileName,
                        TextChangeType = TextChangeType.Deleted, Content = item });
                else
                    result.Add(new TextFileChange() { FirstFileName = file2.FileName, SecondFileName = file1.FileName,
                        TextChangeType = TextChangeType.None, Content = item });

            }

            //added data
            string prevData = default;

            foreach (var item in secondFileData)
            {
                if (!intersectionData.Contains(item))
                    result.Insert(result.ToList().FindIndex(0, (change) => change.Content == prevData) + 1,
                         new TextFileChange() { FirstFileName = file1.FileName, SecondFileName = file2.FileName,
                             TextChangeType = TextChangeType.Appended, Content = item });

                prevData = item;
            }

            if (result.TrueForAll((change) => change.TextChangeType == TextChangeType.None))
                return null;

            return result;
        }

        void FindUniqueFiles(List<FileEntity> firstDirContent, List<FileEntity> secondDirContent,
            List<UnitOfChange> differences)
        {
            var firstDirUniqueFiles = firstDirContent.FindAll((file) => !secondDirContent.Contains(file));

            var secondDirUniqueFiles = secondDirContent.FindAll((file) => !firstDirContent.Contains(file));

            foreach (var uniqueFile in firstDirUniqueFiles)
                differences.Add(AddChange(uniqueFile));

            foreach (var uniqueFile in secondDirUniqueFiles)
                differences.Add(AddChange(uniqueFile));

            firstDirContent.RemoveAll((file) => firstDirUniqueFiles.Contains(file));

            secondDirContent.RemoveAll((file) => secondDirUniqueFiles.Contains(file));
        }

        public UnitOfChange AddChange(FileEntity file)
        {
            switch (file.Type)
            {
                case FileType.Text:
                    return new UnitOfChange()
                    {
                        ChangeType = DifferenceType.UniqueFile,
                        Changes = new[] { new TextFileChange() { FirstFileName = file.FileName } }
                    };

                case FileType.Binary:
                    return new UnitOfChange()
                    {
                        ChangeType = DifferenceType.UniqueFile,
                        Changes = new[] { new FileChangeEntity() { FirstFileName = file.FileName } }
                    };

                default:
                    throw new Exception("Programm work result is trange...");
            }
        }
    }
}
