﻿namespace Entities
{
    public enum DifferenceType
    {
        None,
        UniqueFile,
        BinaryFileDifference,
        TextFileContentDifference
    }
}
