﻿namespace Entities
{
    public class TextFileChange : FileChangeEntity
    {
        public string Content { get; init; }

        public TextChangeType TextChangeType { get; init; }
    }
}
