﻿using System.Linq;

namespace Entities
{
    public class FileEntity
    {
        public string FileName { get; init; }

        public string Content { get; init; }

        public FileType Type { get; set; }


        public override bool Equals(object obj)
        {
            FileEntity file = obj as FileEntity;

            return file.FileName.Split(System.IO.Path.DirectorySeparatorChar).Last()
                == FileName.Split(System.IO.Path.DirectorySeparatorChar).Last();
        }

        public override int GetHashCode()
        => FileName.GetHashCode();
     
    }
}
