﻿using Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileTypeDefiner
{
    public class Determinant
    {
        async Task<(int, FileType)> DefineFileType(string fileContent, int fileIndex)
        {
            return await Task.Run(() =>
            {
                double procentage = 0;

                for (int i = 0; i < fileContent.Length; i++)
                {
                    if (char.IsDigit(fileContent[i]) || char.IsLetter(fileContent[i])
                    || char.IsWhiteSpace(fileContent[i]) || char.IsPunctuation(fileContent[i]))
                        procentage++;
                }

                procentage /= fileContent.Length;

                if (procentage >= 0.9)
                    return (fileIndex, FileType.Text);
                else if (procentage < 0.9 && procentage > 0.1)
                    return (fileIndex, FileType.Binary);

                return (fileIndex, FileType.None);
            });
        }

        public void DefineFileTypes(List<FileEntity> firstDirectoryContent, List<FileEntity> secondDirectoryContent)
        {
            List<Task<(int index, FileType type)>> firstDirTasks = new List<Task<(int, FileType)>>();

            List<Task<(int index, FileType type)>> secondDirTasks = new List<Task<(int, FileType)>>();

            for (int i = 0; i < firstDirectoryContent.Count; i++)
                firstDirTasks.Add(DefineFileType(firstDirectoryContent[i].Content, i));

            for (int i = 0; i < secondDirectoryContent.Count; i++)
                secondDirTasks.Add(DefineFileType(secondDirectoryContent[i].Content, i));

            Task.WaitAll(firstDirTasks.ToArray());

            firstDirTasks.ForEach((task) => firstDirectoryContent[task.Result.index].Type = task.Result.type);

            Task.WaitAll(secondDirTasks.ToArray());

            secondDirTasks.ForEach((task) => secondDirectoryContent[task.Result.index].Type = task.Result.type);
        }
    }
}
