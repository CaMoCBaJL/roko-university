﻿using System.Collections.Generic;
using Entities;
using DAL;
using System.Text.RegularExpressions;
using System;

namespace BL
{
    public class LogicController
    {
        readonly static char[] stringSeparators = { '\r', '\n' };


        public static List<FileValidationData> Search(string sourceDirectory, string filesPattern, string textPattern,
            Func<string, string, int, List<MatchInfo>> searchMethod)
        {
            List<FileValidationData> allMatches = new List<FileValidationData>();

            foreach (var fileData in DAO.GetValidFilesData(sourceDirectory, filesPattern))
            {
                allMatches.Add(new FileValidationData(fileData.Key));

                int counter = 1;

                foreach (var str in fileData.Value.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries))
                    allMatches[^1].Content.AddRange(searchMethod(str, textPattern, counter));

                counter++;
            }

            return allMatches;
        }

        public static List<MatchInfo> StringSearch(string sourceString, string textPattern, int stringNum)
        {
            List<MatchInfo> matches = new List<MatchInfo>();

            string sourceCopy = sourceString.Clone() as string;

            int index = 0;

            while (index < sourceString.Length)
            {
                index = sourceCopy.IndexOf(textPattern, index);

                if (index < 0)
                    return new List<MatchInfo>();

                index += textPattern.Length;

                matches.Add(new MatchInfo(stringNum, index, sourceString));
            }

            return matches;
        }

        public static List<MatchInfo> RegexSearch(string sourceString, string textPattern, int stringNum)
        {
            List<MatchInfo> matches = new List<MatchInfo>();

            foreach (Match match in Regex.Matches(sourceString, textPattern))
                matches.Add(new MatchInfo(stringNum, match.Index, sourceString));

            return matches;
        }

        public static void LogSearchResults(string fileName, List<FileValidationData> searchData)
        => DAO.LogSearchResult(searchData, fileName);

        public static string SearchCommandArgument(string[] arguments, string commandName)
        => arguments[new List<string>(arguments).FindIndex((str) => str == commandName) + 1];
    }
}
