﻿using System.Collections.Generic;
using System.IO;
using Entities;

namespace DAL
{
    public class DAO
    {
        public static Dictionary<string, string> GetValidFilesData(string sourceFolder, string fileMatchPattern)
        {
            Dictionary<string, string> validFiles = new Dictionary<string, string>();

            foreach (var file in new DirectoryInfo(sourceFolder).GetFiles(fileMatchPattern, SearchOption.AllDirectories))
                validFiles.Add(file.FullName, File.ReadAllText(file.FullName));

            return validFiles;
        }

        public static void LogSearchResult(List<FileValidationData> data, string outputFileName)
        {
            using (StreamWriter writer = new StreamWriter(outputFileName))
                data.ForEach((dataItem) => writer.WriteLine(dataItem.ToString() + System.Environment.NewLine));
        }
    }
}
