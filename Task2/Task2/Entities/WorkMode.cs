﻿namespace Entities
{
    [System.Flags]
    public enum WorkMode
    {
        UseText = 1,
        UseLogger = 4,
        UseRegex = 2,
        Help = 8
    }
}
