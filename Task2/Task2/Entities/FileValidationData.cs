﻿using System.Collections.Generic;
using System.Text;
using System;

namespace Entities
{
    public class FileValidationData
    {
        public string FileName { get; }

        public List<MatchInfo> Content { get; }

        public FileValidationData(string fileName)
        {
            FileName = fileName;

            Content = new List<MatchInfo>();
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            result.Append("File: " + FileName + Environment.NewLine);

            foreach (var item in Content)
            {
                result.Append(item.ToString() + Environment.NewLine);
            }

            return result.ToString();
        }
    }
}
