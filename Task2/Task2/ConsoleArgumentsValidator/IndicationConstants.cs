﻿namespace ConsoleArgumentsValidator
{
    sealed class IndicationConstants
    {
        const string FileFormat = "fileName.fileExtention";

        public static readonly string CorrectUseLoggerCommandInput = "(" + CommandSignatures.UseLogger + " " + FileFormat + ")";

        public const string Logger = "logger";

        public const string RegexTextMatcher = "regex text matcher";
    }
}
