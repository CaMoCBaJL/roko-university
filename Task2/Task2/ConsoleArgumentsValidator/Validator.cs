﻿using System.Collections.Generic;
using Entities;
using System.Text.RegularExpressions;

namespace ConsoleArgumentsValidator
{
    public class Validator
    {
        List<string> _arguments;

        public Validator(IEnumerable<string> args) => _arguments = new List<string>(args);

        public WorkMode ValidateArgs()
        {
            WorkMode result = 0;

            if (_arguments.Contains(CommandSignatures.Help1) || _arguments.Contains(CommandSignatures.Help2) || _arguments.Count == 0)
                return WorkMode.Help;

            if (_arguments.Count < 3)
                throw new System.ArgumentException("Too few arguments!");

            if (ValidateOneArgumentCommandInput(
                IndicationConstants.Logger,
                IndicationConstants.CorrectUseLoggerCommandInput,
                _arguments.FindIndex((str) => str == CommandSignatures.UseLogger) + 1,
                RegexPatterns.FilePattern))
            {
                result |= WorkMode.UseLogger;
            }

            if (_arguments[2] != CommandSignatures.UseRegex)
            {
                if (_arguments[2] == CommandSignatures.UseLogger)
                    throw new System.ArgumentException("Wrong argument input!");
                else
                    result |= WorkMode.UseText;
            }
            else
                result |= WorkMode.UseRegex;

            return result;
        }

        public bool ValidateOneArgumentCommandInput(string commandName, string correctInput, int elementIndex, string matchPattern)
        {
            if (elementIndex > 0)
                if (Regex.IsMatch(_arguments[elementIndex], matchPattern))
                    return true;
                else
                    throw new System.ArgumentException($"Wrong parameters input! To use" +
                        $" {commandName} you should add next parameters to Console: {correctInput}");

            return false;
        }

    }
}
