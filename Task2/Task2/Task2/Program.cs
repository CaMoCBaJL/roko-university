﻿using System;
using System.Collections.Generic;
using BL;
using ConsoleArgumentsValidator;
using Entities;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                WorkMode mode = new Validator(args).ValidateArgs();

                string textPatternMatching = string.Empty;

                List<FileValidationData> data = null;

                if (mode.HasFlag(WorkMode.Help))
                {
                    ShowHelp();

                    return;
                }

                if (mode.HasFlag(WorkMode.UseRegex))
                    data = LogicController.Search(args[0], args[1],
                        LogicController.SearchCommandArgument(args, CommandSignatures.UseRegex),
                        LogicController.RegexSearch);
                
                else if (mode.HasFlag(WorkMode.UseText))
                    data = LogicController.Search(args[0], args[1], args[2], LogicController.StringSearch);
                
                if (mode.HasFlag(WorkMode.UseLogger))
                    LogicController.LogSearchResults(LogicController.SearchCommandArgument(args, CommandSignatures.UseLogger), data);

                ShowSearchResult(data);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (System.IO.IOException)
            {
                Console.WriteLine("Can't read from dir");
            }
        }

        static void ShowHelp()
        {
            Console.WriteLine("Hello! To use this app, input should be next:" + Environment.NewLine);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("source_dir file_to_search_template text_to search" + Environment.NewLine);

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Instead of text_to search, you can use:" + Environment.NewLine);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-r some_regular_expression" + Environment.NewLine);

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("To log you search results, you may use:" + Environment.NewLine);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-l fileName.fileExtention.");

            Console.ForegroundColor = ConsoleColor.Gray;
        }

        static void ShowSearchResult(IEnumerable<FileValidationData> data)
        {
            foreach (var item in data)
            {
                Console.WriteLine(item.ToString());

                Console.WriteLine();
            }
        }
    }
}
